const { Pool } = require('pg');

const pool = new Pool({
    user: 'musicbrainz',
    host: 'localhost',
    database: 'musicbrainz',
    password: 'musicbrainz',
    port: 15432,  // prilagoditi...
})

module.exports = {
    query: async (SQL, params) => {
        try {
            const start = Date.now();
            let res = await pool.query(SQL, params);
            const duration = Date.now() - start;
            console.log('executed query', {
                SQL,
                params,
                duration,
                rows: res.rowCount
            })
            return res;
        } catch (error) {
            console.log(error)
        }
    },
}